<!Doctype html>
<html lang="es">
        <head>
                <title><?= empty($title)?'MENSAJEROS.EC':$title ?></title>
                <meta charset="utf-8">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <link rel="shortcut icon" href="<?= base_url('img/favicon.ico') ?>">                
                <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">                
                <link rel="stylesheet" type="text/css" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">        
                <link rel="stylesheet" type="text/css" href="<?= base_url('css/ace.min.css') ?>">
                <link rel="stylesheet" type="text/css" href="<?= base_url('css/admin.css') ?>">
                <link rel="stylesheet" type="text/css" href="<?= base_url('css/chat.css') ?>">
                <script src="//code.jquery.com/jquery-2.1.1.min.js"></script>
                <script src="<?= base_url('js/ace-extra.min.js') ?>"></script>	
                <script src="<?= base_url().'js/frame.js' ?>"></script>
                <?php $this->load->view('predesign/multiselect') ?>
        </head>        
        <body class="no-skin" >
            <div class="row" style="margin-left:0px; margin-right:0px;">
                <div class="col-xs-8">
                    <div class="mensajes">
                        <div id='destinoname'>No has seleccionado ningún contacto para chatear</div>
                        <ul></ul>
                    </div>
                </div>
                <div class="col-xs-4 contactos">
                    <ul>
                        <li class="title"><b>Clientes</b></li>
                        <?php
                            $this->db->select('user.*, clientes.id as repartidores_id');
                            $this->db->join('user','user.id = clientes.user_id');
                            foreach($this->db->get_where('clientes')->result() as $r): 
                        ?>
                            <li><a id='a<?= $r->id ?>' href="javascript:select('<?= $r->id ?>','<?= $r->nombre ?>',1)"><img class="nav-user-photo" src="<?= base_url(empty($this->user->foto)?'assets/grocery_crud/css/jquery_plugins/cropper/vacio.png':'img/fotos/'.$this->user->foto) ?>" alt="<?= $this->user->nombre ?>" /> <?= $r->nombre ?></a></li>
                        <?php endforeach ?>
                            
                        <li class="title"><b>Repartidores</b></li>
                        
                        <?php
                            $this->db->select('user.*, repartidores.id as repartidores_id');
                            $this->db->join('user','user.id = repartidores.user_id');
                            foreach($this->db->get_where('repartidores')->result() as $r): 
                        ?>
                            <li><a id='a<?= $r->id ?>' href="javascript:select('<?= $r->id ?>','<?= $r->nombre ?>',0)"><img class="nav-user-photo" src="<?= base_url(empty($this->user->foto)?'assets/grocery_crud/css/jquery_plugins/cropper/vacio.png':'img/fotos/'.$this->user->foto) ?>" alt="<?= $this->user->nombre ?>" /> <?= $r->nombre ?></a></li>
                        <?php endforeach ?>
                    </ul>
                </div>
            </div>
            <div class="row" style="margin-left:13px; margin-right:0px; margin-top:20px;">
                <form onsubmit="return sendMessage(true)">
                    <div class="input-group input-group-sm">                    
                        <input type="text" class="form-control" placeholder="Escribe tu mensaje aquí" id='field-mensaje'>
                        <a href="javascript:sendMessage(false)" class="input-group-addon" id="sizing-addon3"><i class="fa fa-paper-plane-o"></i></a>
                    </div>
                </form>
            </div>
            <script>                
                var destino = undefined;
                var destinoname = undefined;
                var user_id = <?= $this->user->id ?>;
                var cliente = 0;
                function select(id,destinonames,clientes){
                    $(".contactos a").removeClass('active');
                    $("#a"+id).addClass('active');
                    destino = id;
                    destinoname = destinonames;
                    cliente = clientes;
                    $("#destinoname").html('Estas hablando con <b>'+destinoname+'</b>');
                    getMessages();
                }
                
                function getMessages(){
                    if(destino!==undefined){
                        $.post('<?= base_url('chat/getMessages') ?>',{user_id:<?= $this->user->id ?>,destino:destino,cliente:cliente},function(data){
                            var mensajes = JSON.parse(data);
                            $(".mensajes ul").html('');
                            for(var i in mensajes){
                                var clase = parseInt(mensajes[i].user_id)!==parseInt(user_id)?'class="active"':'';
                                $(".mensajes ul").append('<li '+clase+'>'+mensajes[i].mensaje+'</li>');
                            }
                            setTimeout(getMessages,10000);
                        });
                    }
                }
                
                function sendMessage(retorno){
                    var mensaje = $("#field-mensaje").val();
                    $("#field-mensaje").val('');
                    $.post('<?= base_url('api/sendMessage') ?>',{user_id:user_id,destino:destino,mensaje:mensaje,cliente:cliente},function(data){
                        getMessages();
                    });
                    if(retorno){
                        return false;
                    }
                }
            </script>
        </body>
</html>
