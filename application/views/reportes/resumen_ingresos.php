<?php if(empty($_POST)): ?>
<div>
    <h1 align="center"> Resumen ingresos</h1>
<form action="<?= base_url('reportes/resumen_ingresos') ?>" method="post">
  <div class="form-group">
    <div class='input-group date' id='datetimepicker1'>
        <input type='text' class="form-control" value="<?= date("d-m-Y H:i:s") ?>" name="desde" id="field-desde"/>
        <span class="input-group-addon">
            <span class="glyphicon glyphicon-calendar"></span>
        </span>
    </div>
  </div>  
  <div class="form-group">
    <div class='input-group date' id='datetimepicker2'>
        <input type='text' class="form-control" value="<?= date("d-m-Y H:i:s") ?>" name="hasta" id="field-hasta"/>
        <span class="input-group-addon">
            <span class="glyphicon glyphicon-calendar"></span>
        </span>
    </div>
  </div>
  <button type="submit" class="btn btn-default">Consultar reporte</button>
</form>
</div>
<? $this->load->view('predesign/bootstrapdatepicker'); ?>
<script>
    $('#datetimepicker1,#datetimepicker2').datetimepicker({
            defaultDate: "<?= date("d/m/Y") ?>",
            format: 'DD/MM/YYYY',
            locale: 'es'
        });
</script>
<? $this->load->view('predesign/chosen'); ?>
<?php else: ?>    
        <?php
            $_POST['desde'] = date("Y-m-d",strtotime(str_replace('/','-',$_POST['desde'])));
            $_POST['hasta'] = date("Y-m-d",strtotime(str_replace('/','-',$_POST['hasta'])));
            $where = '';
            $where.= !empty($_POST['desde'])?"WHERE fecha_salida >='".$_POST['desde']."'":'';
            if(!empty($_POST['hasta'])){
                $where.= empty($_POST['desde'])?"WHERE fecha_salida <='".$_POST['hasta']."'":" AND fecha_salida<='".$_POST['hasta']."'";
            }
            $query = "
                select 
                lugar.nombre_lugar as Lugar,
                user.nombre as Cliente,
                fecha_ingreso,
                hora_entrada,
                fecha_salida,
                hora_salida,
                cant_minutos,
                monto
                FROM estacionamiento
                INNER JOIN lugar ON lugar.id = estacionamiento.lugar_id
                INNER JOIN user ON user.id = estacionamiento.user_id
                ".$where." AND monto!=0
            ";
            $ventas = $this->db->query($query);
        ?>
        
    <h1 align="center"> Resumen de facturas para contador</h1>    
    <p style="font-size:12px;"><strong>Desde:</strong> <?= empty($_POST['desde'])?'Todos':$_POST['desde'] ?> <strong>Hasta:</strong> <?= empty($_POST['hasta'])?'Todos':$_POST['hasta'] ?> </p>
    <?php if($ventas->num_rows()>0): ?>
    <table border="0" cellspacing="18" class="table" width="100%" style="font-size:12px;">
        <thead>
                <tr>
                    <?php foreach($ventas->row() as $n=>$v): ?>
                    <th style="text-align:center;"><?= ucwords(str_replace('_',' ',$n)) ?></th>
                    <?php endforeach ?>
                </tr>
        </thead>
        <tbody>
            <?php $totales = array(); ?>
            <?php foreach($ventas->result() as $n=>$c): ?>                
                <tr>
                        <?php foreach($c as $n2=>$v): ?>
                            <?php 
                                if(empty($totales[$n2])){
                                    $totales[$n2] = 0;
                                }
                                if(is_numeric($v)){
                                    $totales[$n2]+=$v;
                                }
                            ?>
                            <td style="text-align:center"><?= $v ?></td>
                        <?php endforeach ?>
                </tr>
            <?php endforeach ?>
            <tr>
                    <?php $n = 0; ?>
                    <?php foreach($ventas->row() as $n2=>$v): ?>                        
                        <td style="text-align:<?= !is_numeric($v)?'center':'right' ?>">
                            <?php 
                                if($n!=0 && $n2!='cedula' && !empty($totales[$n2])){
                                    echo '<b>'.$totales[$n2].'</b>';
                                }elseif($n==0){
                                    echo '<b>TOTALES</b>';
                                }
                            ?>                 
                        </td>
                        <?php $n++; ?>
                    <?php endforeach ?>
            </tr>
        </tbody>
    </table>
    <?php endif ?>
<?php endif; ?>