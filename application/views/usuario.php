<section>    
    <div class='row' style="min-height:700px;">
        <div class="col-xs-12">
            <div class="row" style="text-align:center;">
                <a href="<?= base_url('pedidos/admin/pedidos') ?>"><div class="col-xs-4 well"><i class="fa fa-envelope fa-2x"></i> <br> Mis solicitudes</div></a>
                <a href="<?= base_url('pedidos/admin/favoritos') ?>"><div class="col-xs-4 well"><i class="fa fa-map-marker fa-2x"></i> <br> Mis direcciones</div></a>
                <a href="<?= base_url('seguridad/perfil/edit/'.$this->user->id) ?>"><div class="col-xs-4 well"><i class="fa fa-wrench fa-2x"></i> <br> Mi perfil</div></a>
            </div>
            <?php if(empty($crud)): ?>
            <div></div>
            <?php else: $this->load->view('cruds/'.$crud); ?>
            <?php endif ?>                          
        </div>
    </div>
</section>