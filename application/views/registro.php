<section class="clear:both" id="home-section-5" style="text-align: left">
    <div class="container"><!-- container via hooks -->	
        <div id="page-content-container">	
            <div class="row-fluid">
                <div class="col-xs-12 col-sm-4 col-sm-offset-1">
                    <div class="form-container">
                        <?= $this->load->view('predesign/login') ?>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-sm-offset-1">
                        <?= $output ?>
                </div>
            </div>
        </div>
    </div>
</section>