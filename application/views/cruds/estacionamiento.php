<a class="btn btn-default" href="<?= base_url('reportes/resumen_ingresos') ?>">Imprimir reporte de resumen de ingresos</a>
<?= $output ?>
<script>
    var monto = 0;
    function buscar_monto(){
        var lugar = $("#field-lugar_id").val();
        if(lugar!==''){
            $.post('<?= base_url('estacion/lugar/json_list') ?>',{search_field:'id',search_text:lugar},function(data){
                data = JSON.parse(data);
                monto = data[0].monto_tarifa;
                calcular();
            });
        }
    }
    
    function calcular(){
         var horae = $('#field-hora_entrada').val();
         var horas = $('#field-hora_salida').val();
         var fechae = $('#field-fecha_ingreso').val();
         var fechas = $('#field-fecha_salida').val();
         if(fechae!=='' && fechas!=='' && horae!=='' && horas!==''){
             fechae = fechae.split('-');
             fechas = fechas.split('-');
             fechae = fechae[0]+'-'+fechae[1]+'-'+fechae[2]+' '+horae;
             fechas = fechas[0]+'-'+fechas[1]+'-'+fechas[2]+' '+horas;
             var fechae = new Date(fechae);
             var fechas = new Date(fechas);
             if(fechae>fechas){
                 alert('la fecha de entrada no puede ser menor a la de salida');
             }else{
                 //Calculamos los minutos
                 $("#field-cant_minutos").val(((fechas-fechae)/1000)/60);
             }
             console.log(fechae);
             calcular_monto();
         }
    };
    
    function calcular_monto(){
        var tiempo = parseFloat($("#field-cant_minutos").val());
        //por cada minuto son 1000 gs
        $("#field-monto").val(tiempo*monto);
    }
    $("#field-fecha_ingreso,#field-fecha_salida,#field-hora_entrada,#field-hora_salida").attr('readonly',true);
    buscar_monto();
</script>