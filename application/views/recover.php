<div id="page-hero-container">
    <div class="page-hero" style="background-image:url(<?= base_url('img/paginas/recover.png') ?>)"></div>
    <div class="slideshowShadow"></div>
    <div class="home-header menuontop" id='menu'>
        <div class="col-xs-12" id="homelogo2" style="text-align:center;">
             <a href="<?= site_url('food') ?>">
                <?= img('img/logo.png','width:20%') ?>
            </a>
        </div>
        <div class="col-lg-3 hidden-xs home-logo osLight" id="homelogo">
            <a href="<?= site_url('food') ?>">
                <?= img('img/logo.png') ?>
            </a>
        </div>
        
        <?php $this->load->view('includes/fragmentos/menu'); ?>
    </div>
    <div class="page-caption">
        <div class="page-title">Recuperar contraseña</div>
    </div>
</div>


<div  id="page"  class="page-wrapper">
    <div class="container">
        <div class="row col-sm-4 col-sm-offset-4 col-xs-12" align="center">
            <form action="<?= base_url('registro/forget') ?>" method="post" onsubmit="return validar(this)" role="form" class="form-horizontal">
                <?= !empty($msj)?$msj:'' ?>
                <input type="email" name="email" id="email" data-val="required" class="form-control" value="<?= $_SESSION['email'] ?>" readonly><br/>
                <input type="password" class="form-control" name="pass" id="pass" placeholder="Nuevo Password"><br/>
                <input type="password" class="form-control" name="pass2" id="pass2" placeholder="Repetir Password"><br/>
                <input type="hidden" name="key" value="<?= $key ?>">
                <button type="submit" class="btn btn-success">Recuperar contraseña</button>
            </form>
        </div>
    </div>
</div>
<?php if(isset($this->gamas)): ?>
<?= $this->load->view('includes/footer') ?>
<?php endif ?>
<script>
    $(document).ready(function () {
        $(window).on('scroll', function () {
            if ($(this).scrollTop() > $("#page").position().top - 50)
                $("#menu").addClass('menuonbottom').removeClass('menuontop');
            else
                $("#menu").addClass('menuontop').removeClass('menuonbottom');
        });

        $("a[href*=#]").click(function (e) {
            e.preventDefault();
            var target = $(this).attr('href');
            target = target.split("#");
            target = $("#" + target[1]);

            $('html, body').stop().animate({'scrollTop': parseInt(target.offset().top) - 30}, 900, 'swing');
        })
    });    
</script>