<section class="clear:both" id="home-section-5" style="">
    <div class="container"><!-- container via hooks -->	
        <div id="page-content-container">	
            <div class="row-fluid">
                <div class="col-xs-12 col-sm-4 col-sm-offset-1">
                    <div class="form-container">
                        
                        <?php 
                            $estacionamiento = $this->db->get('lugar');
                            if($estacionamiento->num_rows()>0):
                        ?>
                        <table class="table-bordered table">
                            <thead>
                                <tr><th colspan="2" style="text-align: center">Lugares disponibles</th></tr>
                                <tr><th>Identificador</th><th>Estado</th></tr>
                            </thead>
                            <tbody>
                                <?php foreach($estacionamiento->result() as $e): ?>
                                <tr><td><?= $e->nombre_lugar ?></td><td>
                                 <?= 
                                    $e->estado==0?'<span class="label label-success">Disponible</span>':'<span class="label label-danger">Ocupado</span>'; 
                                 ?></td></tr>
                                <?php endforeach ?>
                            </tbody>
                        </table>
                        <?php endif ?>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-sm-offset-1">
                    <div class="form-container">
                        <div id="mapa" style="width:100%; height:400px;"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script src="//maps.googleapis.com/maps/api/js?key=AIzaSyBAmpz-tft5eyIVdHRZrXcEhj4ykyaC80E&libraries=drawing,places"></script>
<script>
    function init_map(){
                var map = new google.maps.Map(document.getElementById('mapa'), {
                    zoom: <?= $this->db->get('ajustes')->row()->zoom ?>,
                    center: new google.maps.LatLng<?= $this->db->get('ajustes')->row()->mapa ?>,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                });
                
                new google.maps.Marker(
                {
                    position:new google.maps.LatLng<?= $this->db->get('ajustes')->row()->mapa ?>,
                    title:'Nuestra ubicacion',
                    map:map
                });
        }
        $(document).ready(function(){
           init_map(); 
        });
</script>