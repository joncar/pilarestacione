<!DOCTYPE html>
<html lang="es-ES">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
        <title><?= empty($title) ? 'Pilar Estacione' : $title ?></title>        
        <link rel="shortcut icon" href="<?= base_url('img/favicon.png') ?>" type="image/x-icon" />        
        <link rel="shortcut icon" href="public/img/favicon.ico" sizes="256x256"/>        
        <link rel='stylesheet' type='text/css' href='//fonts.googleapis.com/css?family=Lato:400,700,300'>
        <link href='//api.tiles.mapbox.com/mapbox-gl-js/v0.19.1/mapbox-gl.css' rel='stylesheet'/>        
        <link rel="stylesheet" type="text/css" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">        
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">                
        <link rel='stylesheet' type='text/css' href='//fonts.googleapis.com/css?family=Lato:400,700,300'>
        <link rel="stylesheet" type="text/css" href="<?= base_url('css/chat.css') ?>"/>        
        <link rel="stylesheet" type="text/css" href="<?= base_url('css/template/style.css') ?>"/>        
        <link rel="stylesheet" type="text/css" href="<?= base_url('css/formulario_solicitud.css') ?>"/>        
        <script src="//code.jquery.com/jquery-2.1.1.min.js"></script>
        <script src="<?= base_url() . 'js/frame.js' ?>"></script>        
        <?php if(!empty($scripts)): ?>
            <?= $scripts ?>
        <?php else: ?>
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <?php endif ?>
        <script>
            var base_url = '<?= base_url() ?>';
        </script>
    </head>
    <body>      
        <?php $this->load->view('includes/template/header'); ?>
        <div>
            <?php $this->load->view($view); ?>   
        </div>
        <?php $this->load->view('includes/template/footer'); ?>        
    </body>
</html>