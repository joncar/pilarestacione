<div style="border: 1px solid black">
    <div style="text-align: center; border-bottom: 1px solid black;">
        Ticket de pago
    </div>
    <div style="padding:50px">
        <p><b>Cliente: </b> <?= $data->nombre ?></p>
        <p><b>Fecha: </b> <?= date("d/m/Y",strtotime($data->fecha_salida)) ?></p>
        <p><b>Hora entrada: </b> <?= $data->hora_entrada ?></p>
        <p><b>Hora Salida: </b> <?= $data->hora_salida ?></p>
        <p><b>Monto: </b> <?= number_format($data->monto,0,',','.') ?> Gs.</p>
    </div>
</div>