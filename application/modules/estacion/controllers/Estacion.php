<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Estacion extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        public function estacionamiento($x = '', $y = ''){
            if($x!=='imprimir'){
            $crud = $this->crud_function('','');
            if($crud->getParameters()!=='add'){
                $crud->field_type('fecha_salida','string',date("Y-m-d"));
                $crud->field_type('hora_salida','string',date("H:i"));
            }
            if($crud->getParameters()!=='edit'){
                $crud->field_type('fecha_ingreso','string',date("Y-m-d"));
                $crud->field_type('hora_entrada','string',date("H:i"));
            }
            if($crud->getParameters()=='edit'){
                $crud->field_type('fecha_ingreso','string');
                $crud->field_type('lugar_id','hidden');
            }else{
                 $crud->set_relation('lugar_id','lugar','{nombre_lugar}',array('estado'=>0));
                 $crud->field_type('monto','hidden',0);
                 $crud->field_type('cant_minutos','hidden',0);
                 $crud->field_type('fecha_salida','hidden','');
                 $crud->field_type('hora_salida','hidden','');
            }
            if($crud->getParameters()=='list' || $this->user->admin==1){
                $crud->set_relation('user_id','user','{cedula} {nombre}');
            }else{
                $crud->field_type('user_id','hidden',$this->user->id);
            }
            $crud->callback_after_insert(function($post,$primary){
                get_instance()->db->update('lugar',array('estado'=>1),array('id'=>$post['lugar_id']));
            });
            
            $crud->callback_after_update(function($post,$primary){
                if(!empty($post['fecha_salida'])){
                    get_instance()->db->update('lugar',array('estado'=>0),array('id'=>$post['lugar_id']));
                }
            });
            $crud->add_action('<i class="fa fa-print"></i> Imprimir Ticket','',base_url('estacion/estacionamiento/imprimir/').'/');
            $crud->set_lang_string('update_success_message','Registro realizado satisfactoriamente <a href="'.base_url('estacion/estacionamiento/imprimir/'.$y).'">Imprimir Ticket</a>');
            $crud = $crud->render();
            $crud->crud = 'estacionamiento';
            $this->loadView($crud);
            }
            else{
                
                    $this->db->join('user','user.id = estacionamiento.user_id');
                   $data = $this->db->get_where('estacionamiento',array('estacionamiento.id'=>$y))->row();
                $this->load->view('ticket',array('data'=>$data));
            }
        }         
        
        public function lugar(){
            $crud = $this->crud_function('','');
            $crud->field_type('estado','true_false',array('0'=>'<span class="label label-success">Disponible</span>','1'=>'<span class="label label-danger">Ocupado</span>'));
            $crud = $crud->render();
            $this->loadView($crud);
        }
    }
?>
