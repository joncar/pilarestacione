<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        public function ajustes(){
            $crud = $this->crud_function('','');
            $crud->unset_delete()->unset_add();
            $crud->field_type('mapa','map',array('width'=>'300px','height'=>'300px'));
            $crud = $crud->render();
            $this->loadView($crud);
        }                
    }
?>
