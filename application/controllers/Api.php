<?php
require_once APPPATH.'controllers/Main.php';
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, ver, repartidor");
class Api extends Main {
        
        public function __construct()
        {
                parent::__construct();                
                /*if(empty($_SERVER['HTTP_VER'])){          
                    echo 'denied';
                    die();
                } */           
                $this->load->library('grocery_crud');
                $this->load->library('ajax_grocery_crud');
                $this->load->model('seguridadModel');
                $this->load->model('querys');
        }
        
        public function loadView($data = array('view'=>'main'))
        {
             if(!empty($data->output)){
                $data->view = empty($data->view)?'panel':$data->view;
                $data->crud = empty($data->crud)?'user':$data->crud;
                $data->title = empty($data->title)?ucfirst($this->router->fetch_method()):$data->title;
            }
            parent::loadView($data);
        }
        
        protected function crud_function($x,$y,$controller = ''){
            $crud = new ajax_grocery_CRUD($controller);
            $crud->set_theme('bootstrap2');
            $table = !empty($this->as[$this->router->fetch_method()])?$this->as[$this->router->fetch_method()]:$this->router->fetch_method();
            $crud->set_table($table);
            $crud->set_subject(ucfirst($this->router->fetch_method()));            
            $crud->required_fields_array();
            
            if(method_exists('panel',$this->router->fetch_method()))
             $crud = call_user_func(array('panel',$this->router->fetch_method()),$crud,$x,$y);
            return $crud;
        }
        
        public function ajustes(){
            $crud = $this->crud_function('',''); 
            $crud->unset_delete()
                     ->unset_add()
                     ->unset_edit();
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        /**** Repartidores *****/
        
        public function repartidores($x = '',$y = ''){
            if(empty($_POST['lat']) && empty($_POST['lon'])){
                $this->as = array('repartidores'=>'user');
                if(isset($_POST) && !empty($_POST['search_field']) && in_array('password',$_POST['search_field'])){
                    foreach($_POST['search_field'] as $n=>$p){
                        if($p=='password'){
                            $_POST['search_text'][$n] = md5($_POST['search_text'][$n]);
                        }
                    }
                }
                $crud = $this->crud_function('',''); 
                $crud->unset_delete();
                $crud->unset_fields('fecha_registro');            
                $crud->required_fields('email');
                 //Callbacks
                $crud->callback_before_update(function($post,$primary){
                    if(!empty($primary) && $this->db->get_where('user',array('id'=>$primary))->row()->password!=$post['password'] || empty($primary)){
                        $post['password'] = md5($post['password']);                
                    }
                    return $post;
                });

                $crud->callback_column('admin',function($val,$row){
                    $rep = get_instance()->db->get_where('repartidores',array('user_id'=>$row->id));                
                    if($rep->num_rows()>0){
                        $datos = array(
                            'repartidores_id'=>$rep->row()->id
                        );

                        return json_encode($datos);
                    }
                    return '{}';
                });
            }else{
                if(!empty($_POST['email'])){
                    unset($_POST['email']);
                }
                $crud = $this->crud_function('',''); 
                $crud->required_fields('lat','lon');
            }                                   
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        /**** Pedidos *****/
        
        public function pedidos($x = '',$y = ''){            
            $crud = $this->crud_function('','');
            if($crud->getParameters()=='edit'){
                $crud->required_fields('pagado');
            }
            $crud->set_relation('repartidores_id','repartidores','user_id');
            $crud->set_relation('j8c93d82d.user_id','user','{nombre}|{email}|{foto}|{celular}');
            $crud->callback_column('descripcion',function($val,$row){
                $ar = array();
                $ar['descripcion'] = $val;
                $ar['tareas'] = array();
                foreach(get_instance()->db->get_where('pedidos_detalles',array('pedidos_id'=>$row->id))->result() as $d){
                    $ar['tareas'][] = $d;
                }
                return json_encode($ar);
            });
            //$crud->where('pedidos.id','1');
            $crud->unset_delete();
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        public function pedidos_detalles($x = '',$y = ''){            
            $crud = $this->crud_function('','');
            if($crud->getParameters()=='edit'){
                $crud->required_fields('status');
            }
            $crud->callback_after_update(function($post,$id){
               $actualtask = get_instance()->db->get_where('pedidos_detalles',array('id'=>$id));
                if($actualtask->num_rows()>0){
                    $task = get_instance()->db->get_where('pedidos_detalles',array('pedidos_id'=>$actualtask->row()->pedidos_id,'status'=>0));                    
                    if($task->num_rows()==0){
                        get_instance()->db->update('pedidos',array('status'=>5),array('id'=>$actualtask->row()->pedidos_id));
                        //Notificar fin
                        /*if(!empty($post['status']) && $tasks->num_rows()>0){
                            get_instance()->notificar('sucursales',$tasks->row()->sucursales_id,'Pedido Finalizado','Su pedido #'.$tasks->row()->pedidos_id.' Fue finalizado con éxito');
                        }*/
                    }else{
                        get_instance()->db->update('pedidos',array('status'=>4),array('id'=>$actualtask->row()->pedidos_id));
                        //Notificar                        
                        /*if(!empty($post['status']) && $tasks->num_rows()>0){
                            if($tasks->row()->id==$id){ //Si es el primero se notifica recogida
                                get_instance()->notificar('sucursales',$tasks->row()->sucursales_id,'Recolecta Realizada','Su pedido #'.$tasks->row()->pedidos_id.' fue recolectado y esta en camino para su entrega');
                            }else{
                                get_instance()->notificar('sucursales',$tasks->row()->sucursales_id,'Entrega Realizada','Su pedido #'.$tasks->row()->pedidos_id.' registro una entrega en '.$tasks->row()->direccion);
                            }
                        }*/
                    }
                } 
            });
            $crud->unset_delete();
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        public function getUpdates(){
            $this->form_validation->set_rules('repartidores_id','Repartidor','required|integer');
            if($this->form_validation->run()){
                $respuesta = array();
                $repartidor = $this->db->get_where('repartidores',array('id'=>$_POST['repartidores_id']));
                if($repartidor->num_rows()>0){
                    $repartidor = $repartidor->row();
                    //PEDIDOS
                    $respuesta['pedidos'] = array();
                    $this->db->select("pedidos.*, CONCAT('', COALESCE(user.nombre, ''), '|', COALESCE(user.email, ''), '|', COALESCE(user.foto, ''), '|', COALESCE(user.celular, ''), '') as sdfe80649");
                    $this->db->join('clientes','clientes.id = pedidos.clientes_id');
                    $this->db->join('user','user.id = clientes.user_id');
                    foreach($this->db->get_where('pedidos',array('repartidores_id'=>$_POST['repartidores_id'],'pedidos.status > '=>1))->result() as $p){
                        //Descripcion
                        $descr = $p->descripcion;
                        $p->descripcion = array();
                        $p->descripcion['descripcion'] = $descr;
                        $p->descripcion['tareas'] = array();
                        foreach($this->db->get_where('pedidos_detalles',array('pedidos_id'=>$p->id))->result() as $d){
                            $p->descripcion['tareas'][] = $d;
                        }
                        $respuesta['pedidos'][] = $p;
                    }
                    //MENSAJES
                    $respuesta['mensajes'] = array();
                    $mensajes = $this->db->get_where('mensajes',array('user_id'=>$repartidor->user_id));
                    if($mensajes->num_rows()==0){
                        $this->db->insert('mensajes',array('user_id'=>$repartidor->user_id,'mensajes'=>'[]'));                        
                    }else{
                        $respuesta['mensajes'] = $mensajes->row()->mensajes;
                    }
                    
                    //OPERADORES
                    $respuesta['operadores'] = array();
                    foreach($this->db->get_where('user',array('admin'=>1))->result() as $o){
                        $respuesta['operadores'][] = $o;
                    }
                    
                    echo json_encode($respuesta);
                }else{
                    echo '[]';
                }
            }else{
                echo '[]';
            }
        }
        
        function sendMessage(){
            $this->form_validation->set_rules('destino','Destino','required|integer');
            $this->form_validation->set_rules('user_id','Remitente','required|integer');
            $this->form_validation->set_rules('mensaje','Mensaje','required');
            if($this->form_validation->run()){
                //Guardamos en remitente
                $mensaje = $this->db->get_where('mensajes',array('user_id'=>$_POST['user_id']));                                
                if($mensaje->num_rows()>0){
                    $mensajes = (array)json_decode($mensaje->row()->mensajes);                    
                    $men = array('user_id'=>$_POST['destino'],'mensaje'=>$_POST['mensaje'],'destino'=>$_POST['user_id']);
                    if(!empty($_POST['pedido'])){
                        $men['pedido'] = $_POST['pedido'];
                    }
                    $mensajes[] = $men;
                    $this->db->update('mensajes',array('mensajes'=>json_encode($mensajes)),array('user_id'=>$_POST['user_id']));
                }
                else{
                    $mensajes = array();                   
                    $men = array('user_id'=>$_POST['destino'],'mensaje'=>$_POST['mensaje'],'destino'=>$_POST['user_id']); 
                    if(!empty($_POST['pedido'])){
                        $men['pedido'] = $_POST['pedido'];
                    }
                    $mensajes[] = $men;
                    $this->db->insert('mensajes',array('user_id'=>$_POST['user_id'],'mensajes'=>json_encode($mensajes)));
                }

                //Guardamos en destino--- Si es cliente en todos los admin
                if(empty($_POST['cliente'])){
                    $mensaje = $this->db->get_where('mensajes',array('user_id'=>$_POST['destino']));                
                    if($mensaje->num_rows()>0){
                        $mensajes = (array)json_decode($mensaje->row()->mensajes);
                        $men = array('user_id'=>$_POST['destino'],'mensaje'=>$_POST['mensaje'],'destino'=>$_POST['user_id']);
                        if(!empty($_POST['pedido'])){
                            $men['pedido'] = $_POST['pedido'];
                        }
                        $mensajes[] = $men;
                        $this->db->update('mensajes',array('mensajes'=>json_encode($mensajes)),array('user_id'=>$_POST['destino']));
                    }else{
                        $mensajes = array();
                        $men = array('user_id'=>$_POST['destino'],'mensaje'=>$_POST['mensaje'],'destino'=>$_POST['user_id']); 
                        if(!empty($_POST['pedido'])){
                            $men['pedido'] = $_POST['pedido'];
                        }
                        $mensajes[] = $men;
                        $this->db->insert('mensajes',array('user_id'=>$_POST['destino'],'mensajes'=>json_encode($mensajes)));
                    }
                    
                    //Repartidor enviar mensaje por PUSH
                    $this->load->model('querys');
                    $repartidores = $this->db->get_where('repartidores',array('user_id'=>$_POST['destino']));
                    $user = $this->db->get_where('user',array('id'=>$_POST['user_id']));
                    if($repartidores->num_rows()>0 && $user->num_rows()>0){
                        $this->querys->notificar('repartidores',$repartidores->row()->id,$user->row()->nombre,$_POST['mensaje']);
                    }
                }else{
                    foreach($this->db->get_where('user',array('admin'=>1))->result() as $u){
                        $mensaje = $this->db->get_where('mensajes',array('user_id'=>$u->id));                
                        if($mensaje->num_rows()>0){
                            $mensajes = (array)json_decode($mensaje->row()->mensajes);
                            $men =  array('user_id'=>$u->id,'mensaje'=>$_POST['mensaje'],'destino'=>$_POST['user_id']);
                            if(!empty($_POST['pedido'])){
                                $men['pedido'] = $_POST['pedido'];
                            }
                            $mensajes[] = $men;
                            $this->db->update('mensajes',array('mensajes'=>json_encode($mensajes)),array('user_id'=>$u->id));
                        }else{
                            $mensajes = array();
                            $men = array('user_id'=>$u->id,'mensaje'=>$_POST['mensaje'],'destino'=>$_POST['user_id']);
                            if(!empty($_POST['pedido'])){
                                $men['pedido'] = $_POST['pedido'];
                            }
                            $mensajes[] = $men;
                            $this->db->insert('mensajes',array('user_id'=>$u->id,'mensajes'=>json_encode($mensajes)));
                        }
                    }
                }
        }
      }
      
      public function gcm_repartidores($x = '',$y = ''){
            $crud = $this->crud_function('','');
            $crud->callback_before_insert(function($post){
                get_instance()->db->delete('gcm_repartidores',array('repartidores_id'=>$post['repartidores_id'],'tipo'=>$post['tipo']));
                get_instance()->db->delete('gcm_repartidores',array('valor'=>$post['valor']));
            });
            $crud = $crud->render();
            $this->loadView($crud);
      }           
}
/* End of file panel.php */
/* Location: ./application/controllers/panel.php */
