<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once('Panel.php');
class Reportes extends Panel {
        
        function __construct()
        {
                parent::__construct();  
                ini_set('memory_limit','200M');
        }

        function index($url = 'main',$page = 0)
        {
                $this->loadView('reportes');
        }          
        
        public function resumen_ingresos(){
           if(empty($_POST)){
                    $this->loadView('reportes/resumen_ingresos');
            }
            else{
                //$papel = 'A4';
                //$html2pdf = new HTML2PDF('P',$papel,'fr', false, 'ISO-8859-15', array(5,5,5,8));
                //$html2pdf->setDefaultFont('courier');
                //$html2pdf->writeHTML(utf8_decode($this->load->view('reportes/resumen_ingresos',array(),TRUE)));
                //$html2pdf->Output('Resumen de ingresos.pdf');
                $this->load->view('reportes/resumen_ingresos');
            }
        }
        
        /*Cruds*/ 
}
/* End of file panel.php */
/* Location: ./application/controllers/panel.php */